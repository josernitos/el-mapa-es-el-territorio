# ¿Quién sos?

Ahora deberíamos preguntarle a la computadora acerca de ella. No hay un comando `whoru` (quiensos, en inglés), pero hay uno torpemente redactado.

Podemos preguntarle a la computadora por su nombre, acortado en la jerga de moda de la línea de comandos.

En la terminal, escribí:

```
$: uname
```

Ella regresará<sup>1</sup>:

```
Linux (o Darwin... ¿o tal vez DOS?)
```

En otras palabras, solo te da el nombre de sus sistema operativo. Puede que sienta los stickers que le has puesto, y tal vez ha absorbido tus teclazos en su propio sentido de sí misma, pero no sabe cómo expresar esto. La computadora no se conoce a sí misma de la forma en que vos la conocés. Hay una brecha de entendimiento, una boca llena de palabras no dichas. Ya verás que muchos comandos de terminal se sienten como mixtapes emos del midwest de una sola palabra.

---

## uname

La computadora se abre acerca de sí misma

*le gusta* sólo que le llamen

*regresa* tanta información acerca de la computadora como le preguntés, como sistema operativo, versión de hardware, nombre del kernel y más.

---

<sup>1</sup>Lo que verás depende del tipo de computadora que tenés. En una mac, verás «Darwin». En Linux verás «Linux».

---

< [Anterior: ¿Quién soy?](quien-soy.md) | [Siguiente: ¿Dónde estás?](donde-estas.md) >
