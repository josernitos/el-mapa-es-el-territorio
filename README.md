# El mapa es el territorio

Una introducción mágica a la línea de comandos.

Traducción de [The Map is the Territory](https://solarpunk.cool/zines/map-is-the-territory) por Zach y Angelica del [solarpunk magic computer club](https://solarpunk.cool).

## Tabla de contenido

- [Introducción](introduccion.md)
- [Preparación](preparacion.md)
- [Crear el círculo](crear-el-circulo.md)
- [Abrir la terminal](abrir-la-terminal.md)
- [Decir hola](decir-hola.md)
- [Eco](eco.md)
- [¿Quién soy?](quien-soy.md)
- [¿Quién sos?](quien-sos.md)
- [¿Dónde estás?](donde-estas.md)
- [Listar lo que hay aquí](listar-lo-que-hay-aqui.md)
- [Cambiar de dirección](cambiar-de-direccion.md)
- [Comandos como espíritus](comandos-como-espiritus.md)
- [Hacer un altar](hacer-un-altar.md)
- [Hacer eco de una bendición](hacer-eco-de-una-bendicion.md)
- [Hacer una carpeta de hechizos](hacer-una-carpeta-de-hechizos.md)
- [¿Y ahora dónde estará mi pipa?](donde-estara-mi-pipa.md)
- [Hola curl](hola-curl.md)
- [Nuestro hechizo](nuestro-hechizo.md)
- [Reunir el hechizo](reunir-el-hechizo.md)

## Mantenedor

[@elopio](https://gitlab.com/elopio)


---

Traducido con :rainbow: por [JáquerEspeis](https://jaquerespeis.org).

---

[Siguiente: Introduccion](introduccion.md) >
