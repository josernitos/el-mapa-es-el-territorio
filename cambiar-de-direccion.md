# Cambiar de dirección

Preguntar cosas es divertido, pero hemos estado quiet+s por mucho tiempo. Mováááámonos.

En tu terminal, escribí:

```
$: cd Escritorio
```

La terminal no dirá nada, sólo hará lo que le pedís, tal vez resaltando el prompt para indicar que algo cambió. Lo que cambió es que ahora estás en tu Escritorio en lugar de en tu directorio personal.

`cd` significa «cambiar directorio» y es la forma principal para moverse a través de la computadora.

Este cambio muestra la concentración y atención que tu computadora te da durante una sesión de terminal. No gastó nada de tiempo en cháchara-- Nada de «¡Entiendo! vamos a cambiar de directorio. Vaya, ¡lo logramos! Ahora estás en el Escritorio». Sólo nos dio un destello de un guiño, hizo lo indicado, y esperó tranquila por tu siguiente solicitud. Esta concentración puede ser desorientadora -- puede sentirse como si nada está ocurriendo, o que escribiste algo mal -- pero se vuelve liberadora, al saber que estás en un flujo tan estrecho con esta máquina en lugar de trabajar contra ella.

También es probable que hayás notado una cualidad en todos estos comandos: ¡son increíblemente cortos! Bash no es un lenguaje hablado, ni tampoco mucho de uno escrito. Es un lenguaje para mecanografiar. Sus palabras han evolucionado para ser tan fáciles de teclear como es posible, manteniendo significado. Algunas palabras de bash no tienen letras del todo -- una palabra que usaremos toooodooooo el tiempo es `~`, que significa el «directorio personal».

Podemos usar comandos pasados para verificar que nos hemos movido y estar más a gusto con el lenguaje. Escribí `pwd` y verás que es real que estás en el Escritorio. Escribí `ls` para ver los archivos en tu escritorio.

---

## cd

cambia directorios

*le gusta* una ruta de directorio

*regresa* nada. Te mueve a esa ruta de directorio

---

< [Anterior: Listar lo que hay aquí](listar-lo-que-hay-aqui.md) | [Siguiente: Comandos como espíritus](comandos-como-espiritus.md) >