# Decir hola

Escribí lo siguiente en tu terminal, y presioná enter:

```
$: hola
```

Deberías ver una respuesta similar a:

```
bash: hola: orden no encontrada
```

Es una respuesta un poco bizarra después de tu amabilidad, pero notá que no está diciendo que escribiste algo mal. No hay ningún error aquí. En lugar de eso, vos escribiste lo que ella entendió como un comando, buscó su significado y no pudo encontrarlo.

En otras palabras: la computadora no entiende lo que estás diciendo.

En la línea de comandos no le hablamos a la computadora en español. Lo que usamos es una lengua criolla llamada bash<sup>1</sup>. A una frase en bash le llamamos *comando*, y el truco en la terminal es aprender suficientes comandos para moverse de forma fácil en este espacio.

En la terminal, nuestras herramientas principales son la elocuencia y la metáfora. Lo único que nos limita es lo que no sabemos expresar. Al inicio esto será un montón. Pero el lenguaje, como nuestro corazón, se quiere expandir y conforme pasés más tiempo aquí un lenguaje común entre vos y la computadora crecerá, y será hablado en un dialecto que será completamente tuyo.

¡Muy emocionante! Aprendamos un par de frases clave.

---

<sup>1</sup>Para ser precis+s, hay muchos tipos de «shells», cada una con su propio lenguaje, y Bash es uno de estos. Tu computadora podría estar ejecutando una llamada zsh. Bash es el más común, y probablemente lo que estás viendo, entonces no quiero complicar demasiado las cosas.

---

< [Anterior: Abrir la terminal](abrir-la-terminal.md) | [Siguiente: Eco](eco.md) >
