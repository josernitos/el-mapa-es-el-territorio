# ¿Quién soy?

Vamos a aprender más sobre la terminal preguntándole sobre nosotr+s.

En tu terminal, escribí:

```
$: whoami
```

(quiensoyyo, en inglés)

Deberías ver:

```
jaqueeeeer
```

(o, cualquiera que sea tu nombre de usuaria en esta computadora.)

Es bueno saber dónde estás parad+ con tu computadora. Ella tiene un concepto de «vos», pero en realidad no te conoce a *vos*. Todo lo que sabe está basado en cómo te has representado en texto a vos mism+.

---

## whoami

te dice el nombre de usuaria que escogiste

*le gusta* sólo que le llamen

*regresa* sólo tu nombre de usuaria

---

< [Anterior: Eco](eco.md) | [Siguiente: ¿Quién sos?](quien-sos.md) >
