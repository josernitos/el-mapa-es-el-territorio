# Preparación

Para aprovechar esta zine necesitarás una computadora, un espacio tranquilo, y como una hora sin distracciones.

En la computadora necesitarás una terminal, ya que vamos a escribir comandos en ella. La forma de abrir una terminal depende del tipo de computadora que usés.

## En Linux

Linux ama la terminal. No importa cuál «distro» usés, la terminal es probable que destaque. En Ubuntu, por ejemplo, podés encontrarla haciendo clic en la barra de programas y buscando «terminal».

## En Mac

Por suerte, la terminal ya está instalada, solo un poco escondida. Podés encontrarla con spotlight haciendo clic en el icono de la lupa en la barra de menú (o presionando `cmd+space`), y luego buscando «terminal». Otra alternativa es ir a aplicaciones > utilidades > terminal.

## En Windows

Windows puede ser un poco difícil, y con honestidad, no se lo suficiente para ayudar. Si estás en windows 10, podés instalar linux dentro de tu sistema con windows. Aquí podés leer una guía de cómo instalar Ubuntu (una distribución de linux popular):
[Tutorial de Ubuntu para instalar en windows 10](https://discourse.ubuntu.com/t/instalacion-de-ubuntu-en-windows-10/14949)

Si no estás en 10, he oído cosas buenas acerca del programa «git bash». Aquí hay [un tutorial a profundidad de cómo instalar git bash](https://www.stanleyulili.com/git/how-to-install-git-bash-on-windows/) (en inglés)

Recomiendo leer esta zine de una sola sentada. Poné música linda, hacete un té, ¡pasala bien!

---

< [Anterior: Introducción](introduccion.md) | [Siguiente: Crear el círculo](crear-el-circulo.md) >
