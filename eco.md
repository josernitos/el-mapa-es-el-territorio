# Eco

La línea de comandos es un lugar de tranquilidad placentera. Para ser honesto, este fue lo primero que me atrajo... que podía escapar del ruido de la tecnología moderna y aún así seguir usando mi computadora.

Podemos jugar con esta quietud con el comando `echo` (eco, en inglés).

En tu terminal, escribí:

```
$: echo "ecooooo"
```

Esto debería regresar:

```
"ecooooo"
```

Echo hace un eco. Estamos en un lugar tan masivo y tranquilo que podemos oír (= leer) nuestra propia voz.

---

## echo

repite de regreso lo que le das.

*le gusta* texto, en todas sus formas

*regresa* el mismo texto.

---

< [Anterior: Decir hola](decir-hola.md) | [Siguiente: ¿Quién soy?](quien-soy.md) >
