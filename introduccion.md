# Introducción

Bajo la superficie visual de tu computadora hay una magia antigua y poderosa, un flujo de energía silencioso pero veloz del que la computadora toma poder. Esta magia está oculta pero siempre presente, como un pozo sagrado en la base de una catedral.

Este lugar oculto tiene muchos nombres: shell, terminal, bash, zsh, la línea de comandos. Todos estos nombres son correctos, pero incompletos. Precisos a una parte, pero incapaces de describir el todo. Como todas las cosas mágicas, hay aspectos de la línea de comandos que siempre estarán más allá de lo que podemos articular.

También, como con tantas cosas mágicas, el mundo secular siempre trata de deformarla y quitarle sus garras. La cultura tecnológica moderna describe a la línea de comandos como una oscura herramienta de productividad. Una actividad sin sentido que un+ aprende sólo para impresionar a otr+s gentes tec. O una frase sin sentido: para convertirse en un+ «power user». La sabiduría convencional tecnológica te dirá que la línea de comandos es un lugar intimidante, oscuro, imponente. Imposible de aprender y peligrosa de usar. Esto no es más que un intento de esconder su naturaleza real: la línea de comandos es un lugar completamente formado por nuestra primera tecnología oculta, la palabra.

La línea de comandos es un lenguaje puro, y existir en ella es practicar todo el poder de la metáfora y el diálogo para cambiar realidades y manifestar mundos. Es un lugar de empoderamiento, creatividad tangible y desconcierto místico. Aunque puede ser peligrosa, también es sumamente servicial si sabés cómo escuchar.

Este lugar nos espera, pero no se da a conocer hasta que preguntemos por ella. Debés dar el primer paso.

Yo amo la terminal, el shell, bash, la línea de comandos. La descubrí durante un tiempo de agotamiento digital extremo, y este descubrimiento fue, honestamente, una de las partes más importantes de mi vida. Porque vos sos tuanis, y posiblemente mi ami (¡o lo serás en el futuro!), quiero compartir la línea de comandos con vos.

Esta es una introducción a interactuar con tu computadora a través de la terminal, y una insinuación hacia todo el hermoso poder radical posible que hay en ella. Aprenderemos algunas cosas básicas haciendo hechizos, y al final habrás reunido fragmentos mágicos del éter y lanzarás un hechizo desde un modesto altar digital. Al mismo tiempo, esta zine enseña los mismos términos y habilidades que se encuentran en guías técnicas y podés usarlas para cosas prácticas, como de negocios. Esto no le quita a la magia inherente de los comandos, el tarot puede ser muy bueno para juegos de cartas y las hierbas mágicas funcionan muy bien en salsas y sopas.

Espero poder compartir la maravilla y el deleite de la terminal y que esto sea significativo para vos. Decime si es así, y si no, no.

---

< [Anterior: Tabla de contenidos](README.md) | [Siguiente: Preparación](preparacion.md) >