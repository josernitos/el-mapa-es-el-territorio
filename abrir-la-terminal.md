# Abrir la terminal

En la terminal deberías ver... casi nada. Así es como debe ser. No hay notificaciones, no hay ventanitas con consejos diarios, no hay indicaciones de cómo deberías empezar.

![Imagen de una terminal](https://solarpunk.cool/zines/map-is-the-territory/aesthetic/tmitt_terminal.png)

Entonces, para empezar: no hagás nada. Notá cómo la terminal tampoco hace nada. Nada va a aparecer preguntando si necesitás un poco más de tiempo, ninguna ventana de chat con un jovial recordatorio de qué comandos probar. La terminal solo espera. No tiene expectativas, no tiene motivos. No tiene emociones del todo. No espera ni de forma impaciente ni de forma paciente. Simplemente espera <sup>1</sup>.

La parte que hace que la terminal no sea una página en blanco es una línea con texto críptico y un `$`. La línea exacta será diferente en cada terminal. Tal vez veás tu nombre y un `~`, o tal vez el nombre de tu computadora. Podrías ver algo como `bash 3.2$`.

A esto le llamamos «prompt», y es donde podemos ingresar texto para que la computadora responda. Es la parte de la línea, en la línea de comandos.

Usar la terminal es entrar en un diálogo con tu computadora. Le pedís que haga algo y lo hará. La mayoría de las veces no da una respuesta exterior, sólo se mueve de forma rápida y diligente a hacer la tarea, y espera en silencio tu siguiente petición. Si le hacés una pregunta, imprimirá su respuesta en pantalla. Si no te entiende o encuentra un problema, imprimirá su problema de la mejor forma que pueda articularlo.

En todos los casos, cuando veás el prompt, cuando veás un cursor listo para escribir respuestas, podés saber que el diálogo está ocurriendo y que la computadora está esperando por la siguiente cosa que escribás.

Sigamos adelante y empecemos este diálogo.

---

<sup>1</sup>Esta descripción de la terminal está inspirada en Tech Learning Collective, y su increible curso de fundamentos de la línea de comandos.

---

< [Anterior: Crear el círculo](crear-el-circulo.md) | [Siguiente: Decir hola](decir-hola.md) >
