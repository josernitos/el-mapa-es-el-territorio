# Listar lo que hay aquí

Busquemos una mejor idea de dónde estamos exactamente.

En tu terminal, escribí:

```
$: ls
```

Deberías ver algo como:

```
Descargas Documentos Escritorio Fotos
```

Esta es una lista de cada carpeta y archivo en tu directorio personal.

Es probable que veás algunos nombres familiares. En este lugar, incluso tu escritorio es solo una carpeta con algunos archivos. Estás viendo su esencia: un poco de texto en una pantalla negra (o blanca).

---

## ls

regresa los contenidos de una ruta dada

*le gusta* rutas de directorios y archivos

*regresa* los contenidos de las rutas

---

< [Anterior: ¿Dónde estás?](donde-estas.md) | [Siguiente: Cambiar de dirección](cambiar-de-direccion.md) >
