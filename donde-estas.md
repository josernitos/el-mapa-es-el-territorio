# ¿Dónde estás?

Ahora que sabemos cómo nos vemos una a otra, lo siguiente es descubrir dónde estamos.

En tu terminal, escribí:

```
$: pwd
```

Deberías ver algo como:

```
/home/jaqueeeeer
```

(La mía dice `/home/zach`)

En la terminal, la ubicación de un archivo o una persona es solo un poco de texto, con cada carpeta o subcarpeta (también llamadas directorios y subdirectorios) separada por un `/`. Ahora estamos en lo que se conoce como tu *directorio personal* o «home» (casa, en inglés).

Esto no nos da mucha información, aún, más allá de una linda noción de que estamos en casa.

---

## pwd

Imprime el directorio de trabajo (print working directory, en inglés)

*le gusta* ser llamada, ser de utilidad

*regresa* tu ubicación actual

---

< [Anterior: ¿Quién sos?](quien-sos.md) | [Siguiente: Listar lo que hay aquí](listar-lo-que-hay-aqui.md) >
